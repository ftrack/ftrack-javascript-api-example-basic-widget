
*******************************************
ftrack JavaScript API: Basic Widget Example
*******************************************

This is a basic example which shows the use of the JavaScript client for the
ftrack API and the iframe widget interface, to create a custom widget in ftrack
which shows a grid of versions published on the selected entity.

The example is built using vanilla JavaScript to make no assumptions of
framework or libraries used. In a real-world scenario, it might be useful
to leverage an existing framework or libraries.

The file `ftrack_widget.js` encapsulates the logic for communicating with the
ftrack web interface, and `index.js` contains the rest of the logic. The code
is commented and should be fairly easy to follow. Let us know if you have any
questions.

For more information on the APIs used, please refer to the documentation:

* `Building dashboard widgets <http://ftrack.rtd.ftrack.com/en/stable/developing/building_dashboard_widgets.html>`_
* `JavaScript API client <http://ftrack-javascript-api.rtd.ftrack.com/en/stable/>`_
