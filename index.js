'use strict';
(function (ftrack, ftrackWidget) {
    var session = null;

    /** Initialize session with credentials once widget has loaded */
    function onWidgetLoad() {
        var credentials = ftrackWidget.getCredentials();
        session = new ftrack.Session(
            credentials.serverUrl,
            credentials.apiUser,
            credentials.apiKey
        );

        console.debug('Initializing API session.');
        session.initializing.then(function () {
            console.debug('Session initialized');
        });

        onWidgetUpdate();
    }

    /** Query API for name and versions when widget has loaded. */
    function onWidgetUpdate() {
        var entity = ftrackWidget.getEntity();
        console.debug('Querying new data for entity', entity);

        // Query current entity name
        var nameRequest = session.query(
            'select name from ' + entity.type + ' where id is "' + entity.id + '" limit 1'
        );

        // Query versions published on current entity
        var versionRequest = session.query(
            'select id, thumbnail_id, asset.name, asset.type.name, version from AssetVersion where asset.context_id is "' + entity.id + '" limit 50'
        );

        // Wait for both requests to finish, then update interface.
        Promise.all([nameRequest, versionRequest]).then(function (values) {
            updateInterface(values[0].data[0], values[1].data);
        });
    }

    /** Update widget UI and display *entity* and *versions*. */
    function updateInterface(entity, versions) {
        console.debug('Updating interface');
        var wrapper = _createElement('div');
        wrapper.appendChild(headerElement(entity));
        wrapper.appendChild(versionsListElement(versions))
        replaceContents('widget', wrapper);
    }

    /** Return header element */
    function headerElement(entity) {
        return _createElement(
            'h2', null, { textContent: 'Versions published on ' + entity.name }
        );
    }

    /** Return version list element */
    function versionsListElement(versions) {
        if (!versions.length) {
            return _createElement('p', null, { textContent: 'No versions'});
        }

        var versionList = _createElement(
            'ul', null, { className: 'version-list' }
        );
        for (var i = 0; i < versions.length; i += 1) {
            versionList.appendChild(
                versionListItemElement(versions[i])
            );
        }
        return versionList;
    }

    /** Return version list item element */
    function versionListItemElement(version) {
        var asset = version.asset;
        var assetType = asset.type && asset.type.name || 'Version';
        var versionText = asset.name + ' v' + version.version + ' (' + assetType + ')';

        // Create list item with thumbnail as background and text.
        var listItem = _createElement('li', null, { className: 'version-item' });
        listItem.style.backgroundImage = 'url(' + session.thumbnailUrl(version.thumbnail_id) + ')';
        _createElement('p', listItem, { textContent: versionText, className: 'version-item-text' })

        // Store id as data attribute and set up click handler.
        listItem.dataset.versionId = version.id;
        listItem.onclick = onVersionClicked;
        return listItem;
    }

    /** 
     * Create new html element of *tagName*. 
     * 
     * Optionally copy properties, such as *className* from *options*.
     * Append to *parent*, if specified.
     **/
    function _createElement(tagName, parent, options) {
        var el = window.document.createElement(tagName);

        options = options || {};
        for (var property in options) {
            if (options.hasOwnProperty(property)) {
                el[property] = options[property];
            }
        }

        if (parent) {
            parent.appendChild(el);
        }

        return el;
    }

    function replaceContents(elementId, child) {
        var rootElement = window.document.getElementById(elementId);
        while (rootElement.firstChild) {
            rootElement.removeChild(rootElement.firstChild);
        }
        rootElement.appendChild(child);
    }

    /** Open sidebar when a version is clicked. */
    function onVersionClicked(event) {
        console.debug('Clicked version.');
        var targetElement = event.target;
        var versionId = targetElement && targetElement.dataset.versionId;
        if (versionId) {
            ftrackWidget.openSidebar('AssetVersion', versionId);
        }
    }

    /** Initialize widget once DOM has loaded. */
    function onDomContentLoaded() {
        console.debug('DOM content loaded, initializing widget.');
        ftrackWidget.initialize({
            onWidgetLoad: onWidgetLoad,
            onWidgetUpdate: onWidgetUpdate,
        });
    }

    window.addEventListener('DOMContentLoaded', onDomContentLoaded);
}(window.ftrack, window.ftrackWidget));